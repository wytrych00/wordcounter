import json.WordSearchResult;
import models.Word;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import play.test.Fixtures;
import play.test.UnitTest;
import services.TimeSeriesRetriever;

import java.time.Instant;
import java.util.Date;
import java.util.List;

public class TimeSeriesRetrieverTest extends UnitTest {

    @Before
    public void setUp() {
        //Fixtures.deleteDatabase();
        Fixtures.executeSQL("SET GLOBAL TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
        //Fixtures.executeSQL("insert into date_sequence values (1)");
        //Fixtures.executeSQL("insert into word_sequence values (1)");
        Fixtures.loadModels("fixtures/word-data.yaml");
    }

    @Test
    public void validatesParameters() {
        TimeSeriesRetriever retriever = new TimeSeriesRetriever(null, null, null);
        assertFalse(retriever.isValid());
        retriever = new TimeSeriesRetriever("word", null, null);
        assertFalse(retriever.isValid());
        retriever = new TimeSeriesRetriever("word", new Date(0), null);
        assertFalse(retriever.isValid());
        retriever = new TimeSeriesRetriever("word", new Date(1), new Date(0));
        assertFalse(retriever.isValid());
        retriever = new TimeSeriesRetriever("word", new Date(0), new Date(0));
        assertTrue(retriever.isValid());
    }

    @Ignore
    @Test
    public void returnsTimeSeries() {
        Date today = Date.from(Instant.parse("2017-09-03T00:00:00Z"));
        Date tomorrow = Date.from(Instant.parse("2019-09-04T00:00:00Z"));
        TimeSeriesRetriever retriever = new TimeSeriesRetriever("one", today, tomorrow);
        List<Word> word = Word.findAll();
        System.out.println(word.get(0).getWord());
        WordSearchResult result = retriever.getTimeSeries();
        assertEquals("one", result.word);
        assertEquals(1, result.timeSeries.size());
        assertEquals((Object) 4, result.timeSeries.get(0).getCount());

    }

}
