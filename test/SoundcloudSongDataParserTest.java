import org.junit.Test;
import play.test.UnitTest;
import services.JSONException;
import services.SoundcloudSongDataParser;

import java.util.Map;

public class SoundcloudSongDataParserTest extends UnitTest {

    @Test
    public void constructorThrowsWithIncorrectData() {
        String message = null;
        try {
            new SoundcloudSongDataParser("bla");
        } catch (JSONException e) {
            message = e.getMessage();
        } finally {
            assertNotNull(message);
        }
    }

    @Test
    public void parsesCorrectlyFormattedJSON() {
        String JSON = "{" +
                "date: '20150101'," +
                "songs: [" +
                "{ title: 'Some title' }," +
                "{ title: 'Another title' }" +
                "]}";

        SoundcloudSongDataParser parser = null;
        try {
            parser = new SoundcloudSongDataParser(JSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        assertEquals("2015-01-01", parser.getDate().toString());
        Map<String, Integer> wordCounts = parser.getWordCounts();
        assertEquals(3, wordCounts.size());
        assertEquals((Object) 1, wordCounts.get("some"));
        assertEquals((Object) 1, wordCounts.get("another"));
        assertEquals((Object) 2, wordCounts.get("title"));
    }
}
