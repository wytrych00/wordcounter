package services;

import com.google.gson.JsonElement;
import play.libs.WS;

public class SoundcloudSongDataFetcher {
    private final String BASE_URL = "https://api.soundcloud.com";
    private final String ENDPOINT = "/tracks";
    private final String CLIENT_ID =  "3a792e628dbaf8054e55f6e134ebe5fa";

    public void fetch() {
        WS.WSRequest request = WS.url(BASE_URL + ENDPOINT);
        request.setParameter("client_id", CLIENT_ID);

        WS.HttpResponse response;
        try {
            response = request.get();
        } catch (Exception e) {
            return;
        }
        JsonElement jsonResponse = response.getJson();

        SoundcloudSongDataStore soundcloudSongDataStore = new SoundcloudSongDataStore(jsonResponse);
        soundcloudSongDataStore.saveSongData();
    }

}
