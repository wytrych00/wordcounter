package services;

import models.AppearanceDate;
import models.Word;
import java.util.List;

public class WordCountSaver {
    private SoundcloudSongDataParser parser;
    private AppearanceDate appearanceDate;

    public WordCountSaver(SoundcloudSongDataParser parser) {
        this.parser = parser;
    }

    public void save() {
        appearanceDate = AppearanceDate.saveDate(parser.getDate());
        parser.getWordCounts().forEach(this::saveWordWithCount);
    }

    private void saveWordWithCount(String word, Integer count) {
        // When using WySQL Hibernate complains when '$' is in the query: Illegal group reference
        String escapedWord = word.replace("$", "");
        List<Word> wordEntries = Word.find("byWord", escapedWord).fetch();

        Word wordEntry;
        if (wordEntries.size() > 0)
            wordEntry = wordEntries.get(0);
        else
            wordEntry = new Word(escapedWord);

        wordEntry.addAppearanceDateAndCount(appearanceDate, count);
        wordEntry.save();

    }
}
