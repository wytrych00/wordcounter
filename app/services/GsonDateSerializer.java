package jobs;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.sql.Date;

public class GsonDateSerializer implements JsonSerializer<java.sql.Date> {
    @Override
    public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
        String dateString = date.toString().replaceAll("-", "");
        return new JsonPrimitive(dateString);
    }
}