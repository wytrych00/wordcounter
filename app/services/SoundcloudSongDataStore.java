package services;

import com.google.gson.JsonElement;

class SoundcloudSongDataStore {
    private WordCountSaver saver;

    SoundcloudSongDataStore(JsonElement jsonResponse) {
        SoundcloudSongDataParser parser = new SoundcloudSongDataParser(jsonResponse);
        saver = new WordCountSaver(parser);
    }

    void saveSongData() {
        saver.save();
    }
}
