package services;

public class JSONException extends Exception {
    JSONException(String message) {
        super(message);
    }
}