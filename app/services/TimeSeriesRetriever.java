package services;

import json.ErrorResult;
import json.WordSearchResult;
import models.*;

import java.util.*;


public class TimeSeriesRetriever {
    private final String word;
    private java.sql.Date startDate = null;
    private java.sql.Date stopDate = null;

    public TimeSeriesRetriever(String word, java.util.Date startDate, java.util.Date stopDate) {
        this.word = word;
        if (startDate != null)
            this.startDate = new java.sql.Date(startDate.getTime());
        if (stopDate != null)
            this.stopDate = new java.sql.Date(stopDate.getTime());
    }

    public boolean isValid() {
        return getErrorMessage() == null;
    }

    public ErrorResult getErrorMessage() {
        if (startDate == null)
            return new ErrorResult("Bad format for parameter 'from': needs to be in YYYYMMDD format.");
        if (stopDate == null)
            return new ErrorResult("Bad format for parameter 'to': needs to be in YYYYMMDD format.");
        if (stopDate.compareTo(startDate) < 0)
            return new ErrorResult("Start date must be later than or equal to stop date.");
        if (word == null)
            return new ErrorResult("Word must not be empty.");
        return null;
    }

    public WordSearchResult getTimeSeries() {
        return new WordSearchResult(word, getResultsPerDay());
    }

    private List<WordCountTimeSeries> getResultsPerDay() {
        WordCountTimeSeries.ResultFetcher resultFetcher = new WordCountTimeSeries.ResultFetcher(word, startDate, stopDate);
        return resultFetcher.getResults();
    }

}
