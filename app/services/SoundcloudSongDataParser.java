package services;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import json.SongTitlesWithDate;
import json.SoundcludSong;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class SoundcloudSongDataParser {
    private final Map<String, Integer> wordCounts;
    private final Gson gson = new Gson();
    private final java.sql.Date date;

    public SoundcloudSongDataParser(JsonElement jsonResponse) {
        SoundcludSong[] songs = gson.fromJson(jsonResponse, SoundcludSong[].class);
        wordCounts = parseSongs(songs);
        date = new java.sql.Date(System.currentTimeMillis());
    }

    public SoundcloudSongDataParser(String requestBody) throws JSONException {
        SongTitlesWithDate songs;
        try {
            songs = gson.fromJson(requestBody, SongTitlesWithDate.class);
        } catch (Exception e) {
            throw new JSONException("Error parsing request");
        }

        if (songs.songs == null || songs.songs.length == 0)
            throw new JSONException("No songs in request");
        if (songs.date == null)
            throw new JSONException("No date in request");

        wordCounts = parseSongs(songs.songs);
        date = new java.sql.Date(songs.date.getTime());
    }

    private Map<String,Integer> parseSongs(SoundcludSong[] songs) {
        return Arrays.stream(songs)
                .map(song -> song.title.trim().toLowerCase().split(" "))
                .flatMap(Arrays::stream)
                .filter(songTitle -> songTitle.length() != 0)
                .collect(Collectors.toMap(
                        songTitle -> songTitle,
                        songTitle -> 1,
                        Integer::sum
                ));
    }

    public java.sql.Date getDate() {
        return date;
    }

    public Map<String,Integer> getWordCounts() {
        return wordCounts;
    }
}