package json;

import models.WordCountTimeSeries;

import java.util.List;

public class WordSearchResult {
    public final String word;
    public final List<WordCountTimeSeries> timeSeries;

    public WordSearchResult(String word, List<WordCountTimeSeries> timeSeries) {
        this.word = word;
        this.timeSeries = timeSeries;
    }
}
