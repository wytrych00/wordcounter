package json;

import java.util.Date;

public class SongTitlesWithDate {
    public final Date date;
    public final SoundcludSong[] songs;

    public SongTitlesWithDate(Date date, SoundcludSong[] songs) {
        this.date = date;
        this.songs = songs;
    }
}
