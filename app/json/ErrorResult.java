package json;

public class ErrorResult {
    public final String message;

    public ErrorResult(String message) {
        this.message = message;
    }
}
