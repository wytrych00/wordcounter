package metamodels;

import models.Word;
import models.WordDateCount;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Word.class)
public abstract class Word_ {

	public static volatile ListAttribute<Word, WordDateCount> counts;
	public static volatile SingularAttribute<Word, Integer> id;
	public static volatile SingularAttribute<Word, String> word;

	public static final String COUNTS = "counts";
	public static final String ID = "id";
	public static final String WORD = "word";

}

