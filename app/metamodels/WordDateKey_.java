package metamodels;

import models.AppearanceDate;
import models.Word;
import models.WordDateKey;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WordDateKey.class)
public abstract class WordDateKey_ {

	public static volatile SingularAttribute<WordDateKey, AppearanceDate> appearanceDate;
	public static volatile SingularAttribute<WordDateKey, Word> word;

	public static final String APPEARANCE_DATE = "appearanceDate";
	public static final String WORD = "word";

}

