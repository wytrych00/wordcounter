package metamodels;

import models.WordDateCount;
import models.WordDateKey;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WordDateCount.class)
public abstract class WordDateCount_ {

	public static volatile SingularAttribute<WordDateCount, WordDateKey> wordDateKey;
	public static volatile SingularAttribute<WordDateCount, Integer> count;

	public static final String WORD_DATE_KEY = "wordDateKey";
	public static final String COUNT = "count";

}

