package metamodels;

import models.AppearanceDate;
import models.WordDateCount;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AppearanceDate.class)
public abstract class AppearanceDate_ {

	public static volatile SingularAttribute<AppearanceDate, Date> date;
	public static volatile ListAttribute<AppearanceDate, WordDateCount> counts;
	public static volatile SingularAttribute<AppearanceDate, Integer> id;

	public static final String DATE = "date";
	public static final String COUNTS = "counts";
	public static final String ID = "id";

}

