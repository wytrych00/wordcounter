package models;

import play.db.jpa.GenericModel;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class WordDateCount extends GenericModel implements Serializable {
    @EmbeddedId
    private WordDateKey wordDateKey;
    private int count;

    static WordDateCount findByCompositeId(Word word, AppearanceDate appearanceDate) {
        TypedQuery<WordDateCount> wordDateCountTypedQuery = JPA.em().createQuery(
                "from WordDateCount w where w.wordDateKey.word=:word and w.wordDateKey.appearanceDate=:date",
                WordDateCount.class
        );
        wordDateCountTypedQuery.setParameter("word", word);
        wordDateCountTypedQuery.setParameter("date", appearanceDate);
        try {
            return wordDateCountTypedQuery.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(wordDateKey);
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        WordDateCount wordDateCount = (WordDateCount) o;
        return Objects.equals(wordDateKey, wordDateCount.wordDateKey);
    }

    public WordDateKey getWordDateKey() {
        return wordDateKey;
    }

    void setWordDateKey(WordDateKey wordDateKey) {
        this.wordDateKey = wordDateKey;
    }

    void setCount(int count) {
        this.count = count;
    }

    int getCount() {
        return count;
    }
}
