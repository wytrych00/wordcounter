package models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class WordDateKey implements Serializable {
    @ManyToOne
    private Word word;

    @ManyToOne
    private AppearanceDate appearanceDate;

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public AppearanceDate getAppearanceDate() {
        return appearanceDate;
    }

    void setAppearanceDate(AppearanceDate appearanceDate) {
        this.appearanceDate = appearanceDate;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        WordDateKey wordDateKey = (WordDateKey) o;
        return Objects.equals(word, wordDateKey.word) && Objects.equals(appearanceDate, wordDateKey.appearanceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, appearanceDate);
    }

}