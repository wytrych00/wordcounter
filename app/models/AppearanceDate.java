package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class AppearanceDate extends GenericModel implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "date-sequence-generator"
    )
    @SequenceGenerator(
            name = "date-sequence-generator",
            sequenceName = "date_sequence"
    )
    @Column(name = "DATE_ID")
    private Integer id;

    private Date date;

    @OneToMany(
            mappedBy = "wordDateKey.appearanceDate",
            cascade = CascadeType.ALL
    )
    private List<WordDateCount> counts = new ArrayList<>();

    private AppearanceDate(Date date) {
        this.date = date;
    }

    public static AppearanceDate saveDate(Date today) {
        AppearanceDate appearanceDate;
        List<AppearanceDate> existingAppearanceDate = AppearanceDate.find("byDate", today).fetch();
        if (existingAppearanceDate.size() > 0) {
            appearanceDate = existingAppearanceDate.get(0);
        } else {
            appearanceDate = new AppearanceDate(today);
            appearanceDate.save();
        }
        return appearanceDate;
    }

    List<WordDateCount> getCounts() {
        return counts;
    }

    public void setCounts(List<WordDateCount> counts) {
        this.counts = counts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        AppearanceDate appearanceDate = (AppearanceDate) o;
        return Objects.equals(date, appearanceDate.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }
}
