package models;

import org.hibernate.annotations.NaturalId;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(
        indexes = @Index(
            columnList = "word"
        )
)
public class Word extends GenericModel implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "word-sequence-generator"
    )
    @SequenceGenerator(
            name = "word-sequence-generator",
            sequenceName = "word_sequence"
    )
    @Column(name = "WORD_ID")
    private Integer id;

    @NaturalId
    private String word;

    @OneToMany(
            mappedBy = "wordDateKey.word",
            cascade = CascadeType.ALL
    )
    private List<WordDateCount> counts = new ArrayList<>();

    public Word(String word) {
        this.word = word;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public List<WordDateCount> getCounts() {
        return counts;
    }

    public void setCounts(List<WordDateCount> counts) {
        this.counts = counts;
    }

    public void addAppearanceDateAndCount(AppearanceDate appearanceDate, int count) {
        WordDateCount wordDateCount = null;

        if (this.isPersistent())
            wordDateCount = WordDateCount.findByCompositeId(this, appearanceDate);

        if (wordDateCount != null)
            increaseCount(wordDateCount, count);
        else
            createNewDateCount(appearanceDate, count);
    }

    private void increaseCount(WordDateCount wordDateCount, int count) {
        wordDateCount.setCount(wordDateCount.getCount() + count);
        wordDateCount.save();
    }

    private void createNewDateCount(AppearanceDate appearanceDate, int count) {
        WordDateKey wordDateKey = new WordDateKey();
        wordDateKey.setWord(this);
        wordDateKey.setAppearanceDate(appearanceDate);
        WordDateCount wordDateCount = new WordDateCount();
        wordDateCount.setWordDateKey(wordDateKey);
        wordDateCount.setCount(count);
        counts.add(wordDateCount);
        appearanceDate.getCounts().add(wordDateCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        Word word = (Word) o;
        return Objects.equals(this.word, word.word);
    }
}
