package models;

import metamodels.AppearanceDate_;
import metamodels.WordDateCount_;
import metamodels.WordDateKey_;
import metamodels.Word_;
import play.db.jpa.JPA;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Date;
import java.util.List;

public class WordCountTimeSeries {
    public static class ResultFetcher {
        private final String word;
        private final Date startDate;
        private final Date stopDate;
        private TypedQuery<WordCountTimeSeries> query;
        private ParameterExpression<String> wordParameter;
        private ParameterExpression<Date> startDateParameter;
        private ParameterExpression<Date> stopDateParameter;

        public ResultFetcher(String word, Date startDate, Date stopDate) {
            this.word = word;
            this.startDate = startDate;
            this.stopDate = stopDate;

            prepareQuery();
        }

        private void prepareQuery() {
            /*
                Build JPA criteria equivalent to:
                    select w.count, b.date from WordDateCount w
                    join w.wordDateKey.word a
                    join w.wordDateKey.appearanceDate b
                    where a.word = :word
                    and b.date >= :startDate
                    and b.date < :stopDate
             */

            CriteriaBuilder builder = JPA.em().getCriteriaBuilder();

            CriteriaQuery<WordCountTimeSeries> criteria = builder.createQuery(WordCountTimeSeries.class);
            Root<WordDateCount> root = criteria.from(WordDateCount.class);
            Join<WordDateKey, Word> wordJoin = root.join(WordDateCount_.WORD_DATE_KEY).join(WordDateKey_.WORD);
            Join<WordDateKey, AppearanceDate> dateJoin = root.join(WordDateCount_.WORD_DATE_KEY).join(WordDateKey_.APPEARANCE_DATE);

            Path<Integer> countPath = root.get( WordDateCount_.COUNT);
            Path<java.sql.Date> datePath = dateJoin.get(AppearanceDate_.DATE);
            criteria.select(builder.construct(WordCountTimeSeries.class, countPath, datePath));

            wordParameter = builder.parameter(String.class);
            startDateParameter = builder.parameter(Date.class);
            stopDateParameter = builder.parameter(Date.class);
            criteria.where(builder.and(
                    builder.equal(wordJoin.get(Word_.WORD), wordParameter),
                    builder.greaterThanOrEqualTo(dateJoin.get(AppearanceDate_.DATE), startDateParameter),
                    builder.lessThanOrEqualTo(dateJoin.get(AppearanceDate_.DATE), stopDateParameter)
            ));

            query = JPA.em().createQuery(criteria)
                    .setParameter(wordParameter, word)
                    .setParameter(startDateParameter, startDate)
                    .setParameter(stopDateParameter, stopDate);
        }

        public List<WordCountTimeSeries> getResults() {
            return query.getResultList();
        }
    }

    private final Integer count;
    private final java.sql.Date date;

    public WordCountTimeSeries(Integer count, java.util.Date date) {
        this.count = count;
        this.date = new java.sql.Date(date.getTime());
    }

    public WordCountTimeSeries(Integer count, java.sql.Date date) {
        this.count = count;
        this.date = date;
    }

    public static List<WordCountTimeSeries> getResults(String word, Date startDate, Date stopDate) {
        return null;
    }

    public Integer getCount() {
        return count;
    }

    public Date getDate() {
        return date;
    }
}
