package controllers;

import play.mvc.Controller;

public class FourOhFour extends Controller {
    public static void index() {
        response.status = 404;
        renderJSON("404 - Not found");
    }
}