package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jobs.GsonDateSerializer;
import play.data.binding.As;
import play.mvc.Controller;
import services.JSONException;
import services.SoundcloudSongDataParser;
import services.TimeSeriesRetriever;
import services.WordCountSaver;


public class WordController extends Controller {
    private final Gson gson = new GsonBuilder()
            .disableHtmlEscaping()
            .registerTypeAdapter(java.sql.Date.class, new GsonDateSerializer())
            .create();

    public void getSongTimeSeriesWithWord(String word, @As("yyyyMMdd") java.util.Date from, @As("yyyyMMdd") java.util.Date to) {
        TimeSeriesRetriever timeSeriesRetriever = new TimeSeriesRetriever(word, from, to);
        if (!timeSeriesRetriever.isValid()) {
            response.status = 400;
            renderJSON(gson.toJson(timeSeriesRetriever.getErrorMessage()));
        }
        renderJSON(gson.toJson(timeSeriesRetriever.getTimeSeries()));
    }

    public void saveSongs() {
        String requestBody = params.get("body");
        SoundcloudSongDataParser parser = null;
        try {
            parser = new SoundcloudSongDataParser(requestBody);
        } catch (JSONException e) {
            response.status = 400;
            renderJSON(gson.toJson(e.getMessage()));
        }

        WordCountSaver saver = new WordCountSaver(parser);
        saver.save();

        renderJSON(gson.toJson("OK"));
    }

}
