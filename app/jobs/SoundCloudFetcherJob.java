package jobs;

import play.jobs.Job;
import play.jobs.OnApplicationStart;
import services.SoundcloudSongDataFetcher;

@OnApplicationStart
public class SoundCloudFetcherJob extends Job {
    private SoundcloudSongDataFetcher fetcher = new SoundcloudSongDataFetcher();

    public SoundCloudFetcherJob() {
        Integer noOfHours = 1;
        every(noOfHours * 60 * 60);
    }

    @Override
    public void doJob() {
        fetcher.fetch();
    }

}
