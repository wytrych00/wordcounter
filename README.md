# Word count series parser

This project processes input from soundcloud api to store the counts of words in song titles appearing each day.
To make the project gather more data, the song titles are fetched each hour and any duplicates are summed.

There is also a possibility to POST to the `/word/` endpoint with a structure like so:

```
{
  date: "YYYYMMDD",
  songs: [
    {
      title: "Song title"
    },
    ...
  ]
}
```

Please note that POSTing with data containing a few hundred unique words might take around 30s, so be patient.

~~An example URL to get the word counts looks like this: http://wolniewicz.work/word/?word=the&from=20100913&to=20200913~~

~~The project is deployed on Google Cloud (using Kubernetes). It's written in Play Framework 1, using JPA Annotations for table relations mapping, Hibernate for the persistance layer, and WySQL for the database.~~
